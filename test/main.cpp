/* 
 * File:   main.cpp
 * Author: jiangkai
 *
 * Created on April 12, 2013, 2:37 AM
 */

#include <cstdlib>
#include<cstdio>
using namespace std;

int main(int argc, char** argv) {
    int a[5]={1,2,3,4,5};
    int t;
    scanf("%d",&t);
    for(int i=1;i<5;i++){
        a[i]=a[i]+a[i-1];
    }
    for(int i=0;i<5;i++)
        printf("%d ",a[i]);
    return 0;
}

