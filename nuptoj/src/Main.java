/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.math.BigInteger;
import java.util.*;
import java.math.*;

/**
 *
 * @author jiangkai
 */
public class Main{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner cin=new Scanner(System.in);
        Main vbn = new Main();
        String a=cin.next(),b=cin.next();
        String result = vbn.doAdd(a, b);
        System.out.println(result);
    }
    String doAdd(String a, String b) {
		String str = "";
		int lenA = a.length();
		int lenB = b.length();
		int maxLen = (lenA > lenB) ? lenA : lenB;
		int minLen = (lenA < lenB) ? lenA : lenB;
		String strTmp = "";
		for (int i = maxLen - minLen; i > 0; i--) {
			strTmp += "0";
		}
		if (maxLen == lenA) {
			b = strTmp + b;
		} else
			a = strTmp + a;
		int JW = 0;
		for (int i = maxLen - 1; i >= 0; i--) {
			int tempA = Integer.parseInt(String.valueOf(a.charAt(i)));
			int tempB = Integer.parseInt(String.valueOf(b.charAt(i)));
			int temp;
			if (tempA + tempB + JW >= 10 && i != 0) {
				temp = tempA + tempB + JW - 10;
				JW = 1;
			} else {
				temp = tempA + tempB + JW;
				JW = 0;
			}
			str = String.valueOf(temp) + str;
		}
		return str;
	}
}

