<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <form action="form.html">
            <input type="submit" value="form"/>
            <input type="reset" value="reset"/>
            <input type="button" value="hello"/>
        </form>
        <?php
        date_default_timezone_set('PRC');
        
        echo "<p>index test t ime ".date('H:i:s A, jS F Y e I P Z T')."</p>";
        $script_ts= date_default_timezone_get();
        
        if (strcmp($script_ts, ini_get('date.timezone'))){
            echo 'Script timezone differs from ini-set timezone.';
        } else {
            echo 'Script timezone and ini-set timezone match.';
        }
        ?>
    </body>
</html>
