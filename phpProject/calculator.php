<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php ;echo "calculator ".date('Y-M-d');?></title>
        <link href="calculator.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php
            echo "<p>".date('H:m:s A Y-M-d');
                if ( date(H)<12 && date(H)>=0 )
                    echo ' 早上好</p>';
                elseif(date(H)>=12 && date(H)<=13)
                    echo ' 中午好</p>';
                elseif (date(H)>13 && date(H)<=18)
                    echo ' 下午好</p>';
                elseif ( date(H)>18)
                    echo ' 晚上好</p>';
        ?>
        <?php
        if (isset($_POST["number1"]) && isset($_POST["number2"])){
            if(empty($_POST["number1"])){
                echo "<font color='red' align='center'>第一数不能为空</font><br>";
                unset($_POST["sub"]);
            }
            if(empty($_POST["number2"])){
                echo "<font color='red' align='center'>第二数不能为空</font><br>";
                unset($_POST["sub"]);
            }
        }
        $num1=$_POST["number1"];
        $num2=$_POST["number2"];
        $oper=$_POST["oper"];
        if ( $oper == "/" || $oper== "%"){
            if ($num2 == 0){
                echo "<font color='red' >除数不能为0</font><br>";
                unset($_POST["sub"]);
            }
        }
        ?>
        <form action="" method="post" >
            <table  border="0" align="center">
                <caption> <font size="20px" color="blue" align="center"><b>simple calculator</b></font> </caption>
                <tr>
                    <td>
                        <input type="text" size="5" name="number1" value="<?php if(!empty($_POST[number1])) echo "$_POST[number1]";?>"/>
                    </td>
                    <td>
                        <select name="oper">
                            <option value="not_choose">--please choose--</option>
                            <option value="+"<?php if ($_POST["oper"] == "+") echo 'selected';?> >+</option>
                            <option value="-"<?php if ($_POST["oper"] == "-") echo 'selected';?>>-</option>
                            <option value="*"<?php if ($_POST["oper"] == "*") echo 'selected';?>>*</option>
                            <option value="/"<?php if ($_POST["oper"] == "/") echo 'selected';?>>/</option>
                            <option value="%"<?php if ($_POST["oper"] == "%") echo 'selected';?>>%</option>
                        </select>
                    </td>
                    <td>
                        <input type="text" size="5" name="number2" value="<?php if(!empty($_POST[number2])) echo "$_POST[number2]";?>"/>
                        
                    </td>
                    <td>
                        <input type="submit" name="sub" value="calcultor"/>
                    </td>
                </tr>
                <?php
                    if (isset($_POST["sub"]) && !empty($_POST["sub"])){
                        $sum=0;
                        switch($oper){
                            case "+":
                                $sum=$num1+$num2;
                                break;
                            case "-":
                                $sum=$num1-$num2;
                                break;
                            case "*":
                                $sum=$num1*$num2;
                                break;
                            case "/":
                                $sum=$num1/$num2;
                                break;
                            case "%":
                                $sum=$num1%$num2;
                                break;
                        }
                        echo "<tr><td colspan='4' align='center'>";
                        echo "result is:  <font size='20px' color='purple'><b><u><i>$sum</font>";
                        echo "</td></tr>";
                    }
                    ?>
            </table>
        </form>
    </body>
</html>
