package javafxapplication2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class JavaFXApplicationHeart extends Application {

    @Override
    public void start(Stage primaryStage) {
        int width, height;
        Canvas canvas = new Canvas(350, 350);
        width = (int) canvas.getWidth();
        height = (int) canvas.getHeight();

        GraphicsContext gc = canvas.getGraphicsContext2D();
        double x, y, r;
        for (int i = 0; i <= 90; i++) {
            for (int j = 0; j <= 90; j++) {
                //转换为直角坐标系，设置偏移量，使图像居中
                r = Math.PI / 45 * i * (1 - Math.sin(Math.PI / 45 * j)) * 19;
                x = r * Math.cos(Math.PI / 45 * j) * Math.sin(Math.PI / 45 * i) + width / 2;
                y = -r * Math.sin(Math.PI / 45 * j) + height / 4;

                gc.setFill(Color.BLUE);
                gc.fillOval(x, y, 2, 2);
                gc.fillOval(x, y, 1, 1);
            }
        }


        StackPane root = new StackPane();
        root.getChildren().add(canvas);

        Scene scene = new Scene(root, Color.BLACK);

        primaryStage.setTitle("r=a(1-sinθ)");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
