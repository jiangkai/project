/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.*;
import java.math.*;
/**
 *
 * @author jiangkai
 */
public class Main {

    public static boolean isPrime(int n){
        int p=(int)Math.sqrt(n);
        for(int i=2;i<=p;i++){
            if (n % i == 0)
                return false;
        }
        return true;
    }
        
    public static boolean isXun(String t){
        int length=t.length();
        for(int i=0;i<=length/2;i++){
            if(t.charAt(i)!=t.charAt(length-1-i))
                return false;
        }
        return true;
    }
    public static void main(String[] args)throws Exception{
        Scanner cin=new Scanner(System.in);
        
        while(cin.hasNext()){
            String a=cin.next();
            int b=Integer.parseInt(a);
            if(isXun(a) && isPrime(b))
                System.out.print("Yes\n");
            else
                System.out.print("No\n");
        }
    }
}
