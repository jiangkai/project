/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java24hours;

/**
 *
 * @author jiangkai
 */
import java.awt.*;

class PointTester {
    public static void main(String[] args){
        Point object1 = new Point(11,22);
        Point3D object2= new Point3D(7,6,63);
        System.out.println("the 2D point is located at("+object1.x+","+object1.y+")");
        object1.move(4, 14);
        System.out.println(object1.x+" "+object1.y);
        object1.translate(-10, -14);
        System.out.println("the 2D point is located at("+object1.x+","+object1.y+")");
        System.out.println("the 3D point is located at("+object2.x+","+object2.y+","+object2.z+")");
        object2.move(3, 4, 5);
        System.out.println("the 3D point is located at("+object2.x+","+object2.y+","+object2.z+")");
        object2.translate(-10, -10, -10);
        System.out.println("the 3D point is located at("+object2.x+","+object2.y+","+object2.z+")");
    }
}
