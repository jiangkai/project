/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java24hours;
import java.sql.*;
/**
 *
 * @author jiangkai
 */
public class ConnectToMySql {
    public static void main(String[] args){
        String JDriver="com.mysql.jdbc.Driver";
        String conURL="jdbc:mysql://localhost:3306/test";
        String[] id ={"0002","0003"};
        int[] score={89,60};
        try{
            Class.forName(JDriver);   
        }
        catch(java.lang.ClassNotFoundException e){
            System.out.println("ForName :"+e.getMessage());
        }
        try{
            Connection con=DriverManager.getConnection(conURL,"root","921211");
            Statement s=con.createStatement();
//            String r1="insert into student values("+"'0001','wangming',80)";
//            String r2="insert into student values("+"'0002','wangmin',70)";
//            String r3="insert into student values("+"'0003','wangmi',90)";
//            String r4="insert into student values("+"'0004','wangm',86)";
//            String r5="insert into student values("+"'0005','wang',85)";
//            s.executeUpdate(r1);
//            s.executeUpdate(r2);
//            s.executeUpdate(r3);
//            s.executeUpdate(r4);
//            s.executeUpdate(r5);
            PreparedStatement ps =con.prepareStatement("update student set score= ? where id=?");
            int i=0,idlen=id.length;
            do{
                ps.setInt(1, score[i]);
                ps.setString(2, id[i]);
                ps.executeUpdate();
                ++i;
            }while(i<idlen);
            ResultSet rs=s.executeQuery("select * from student");
            while(rs.next()){
                System.out.println(rs.getString("id")+"\t"+rs.getString("name")+"\t"+rs.getString("score"));
            }
            s.close();
            con.close();
        }
        catch(SQLException e){
            System.out.println("SQLException:"+e.getMessage());
        }
    }
}
