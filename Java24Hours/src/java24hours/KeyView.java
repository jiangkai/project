/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package java24hours;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author jiangkai
 */
public class KeyView extends JFrame implements KeyListener {
    JTextField keyText =new JTextField (80);
    JLabel keyLabel = new JLabel("press key in the text field");
    KeyView(){
        super("keyview");
        setLookAndFeel();
        setSize(350,100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        keyText.addKeyListener(this);
        BorderLayout bord = new BorderLayout();
        setLayout(bord);
        add(keyLabel,BorderLayout.NORTH);
        add(keyText,BorderLayout.CENTER);
        setVisible(true);
    }
    @Override
    public void keyTyped(KeyEvent input) {
        char key= input.getKeyChar();
        keyLabel.setText("you pressed "+key);
        
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void keyPressed(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void keyReleased(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void setLookAndFeel() {
        try{
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        }catch (Exception exc){
            
        }
       // throw new UnsupportedOperationException("Not yet implemented");
    }
    public static void main(String[] args){
        KeyView frame= new KeyView();
        
    }
}
